#include <stdlib.h>
#include <stdio.h>

#include "/home/antonin/Documents/C/TD/lib/TD fonctions/utils/MACRO_REF.h"

INT fois_deux(INT a); //Fonction qui multiplie un entier par deux

INT divise_deux(INT a); //Fonction qui divise un entier par deux

VOID apply(INT FUNC(f) (INT), INT tab[], INT tabLength); //Fonction qui applique une fonction a un tableau

VOID afficher_tab(INT tab[], INT tabLength); //Fonction qui affiche les elements du tableau en console

BOOLEAN test_pair(INT a); //Fonction de test si un entier est pair

BOOLEAN test_impair(INT a); //Fonction de test si un entier est impair

BOOLEAN test_premier(INT a); //Fonction de test si un entier est premier

BOOLEAN test_negatif(INT a); //Fonction de test si un entier est negatif

VOID supprime_tab(INT tab[], UINT size, BOOLEAN FUNC(f) (INT), INT** resultTab, UINT* resultSize); //Fonction qui supprime les elements du tableau en fonctiono d'une fonction

VOID tri(INT tab[], INT tabLength); //Fonction de tri des entiers

VOID tri_float(FLOAT tab[], INT tabLength); //Fonction de tri des floats

VOID afficher_tabFloat(FLOAT tab[], INT tabLength);



