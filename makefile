COMPILER=gcc
FLAGS=-Wall -std=c99
OUTPUT=main.out

all:
	${COMPILER} td.c main.c -o ${OUTPUT} ${FLAGS}