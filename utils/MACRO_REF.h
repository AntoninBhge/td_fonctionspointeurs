/** Date: 2020-02-12
 *  File: MOD_REF.h
 *  Author: Antonin Béhague
 *  Desc: Base lib header file
*/

#pragma once
#ifndef MOD_REF

    #include <stdlib.h>

    #define MOD_REF
    
    #define NEW(type)           ((type*)malloc(sizeof(type)))
    #define DELETE(ptr)         (free(ptr))
    #ifndef NULL
        #define NULL (void*)0
    #endif

    #define FUNC(fnc)           (*fnc)
    #define CASTV(var, type)    (*(type*)var)
    #define FLAGS(num)           (printf("Flag #%d\n",num))            /* DEBUG ONLY */

    #define UNUSED(x) (void)(x)

    #define FALSE 0
    #define TRUE 1

    typedef void            VOID;

    typedef signed char     INT8;
    typedef signed short    INT16;
    typedef signed int      INT32;
    typedef signed long     INT64;

    typedef unsigned char   UINT8;
    typedef unsigned short  UINT16;
    typedef unsigned int    UINT32;
    typedef unsigned long   UINT64;

    typedef unsigned char   UCHAR;
    typedef unsigned short  USHORT;
    typedef unsigned int    UINT;
    typedef unsigned long   ULONG;

    typedef int             INT;
    typedef char            CHAR;
    typedef short           SHORT;
    typedef float           FLOAT;
    typedef long            LONG;
    typedef double          DOUBLE;

    typedef CHAR*           STRING;
    typedef UCHAR           BYTE;

    typedef int             BOOL;
    typedef BYTE            BOOLEAN;

#endif
