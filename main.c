#include "td.h"

int main(int argc, char const *argv[])
{
    INT tableau[] = {1,6,-3,4,5};
    FLOAT tableauFloat[] = {1.5, 5.9, 9.4, 3.8, 2.2};
    INT tabLength = 5;

    INT* pairTab = NULL;
    INT* impairTab = NULL;
    INT* premierTab = NULL;
    INT* negatifTab = NULL;

    UINT pairTabSize = 0;
    UINT impairTabSize = 0;
    UINT premierTabSize = 0;
    UINT negatifTabSize = 0;

    //EXERCICE 1
    printf("\n######  EXERCICE 1 #####\n");

    apply(fois_deux, tableau, tabLength);
    afficher_tab(tableau, tabLength);

    apply(divise_deux, tableau, tabLength);
    afficher_tab(tableau, tabLength);

    //EXERCICE 2
    printf("\n######  EXERCICE 2 #####\n");

    printf("\nSUPPRESSION DES NOMBRES PAIRES\n");
    supprime_tab(tableau, tabLength, test_pair, &pairTab, &pairTabSize);
    afficher_tab(pairTab, pairTabSize);

    printf("\nSUPPRESSION DES NOMBRES IMPAIRES\n");
    supprime_tab(tableau, tabLength, test_impair, &impairTab, &impairTabSize);
    afficher_tab(impairTab, impairTabSize);

    printf("\nSUPPRESSION DES NOMBRES PREMIERS\n");
    supprime_tab(tableau, tabLength, test_premier, &premierTab, &premierTabSize);
    afficher_tab(premierTab, premierTabSize);

    printf("\nSUPPRESSION DES NOMBRES NEGATIFS\n");
    supprime_tab(tableau, tabLength, test_negatif, &negatifTab, &negatifTabSize);
    afficher_tab(negatifTab, negatifTabSize);

    DELETE(pairTab);
    DELETE(impairTab);
    DELETE(premierTab);
    DELETE(negatifTab);

    //EXERCICE 3
    printf("\n######  EXERCICE 3 #####\n");

    printf("\nTRI DU TABLEAU DE INT\n");
    tri_int(tableau, tabLength);
    afficher_tab(tableau, tabLength);
    printf("\nTRI DU TABLEAU DE FLOAT\n");
    tri_float(tableauFloat, tabLength);
    afficher_tabFloat(tableauFloat, tabLength);

}
