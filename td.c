#include "td.h"
#include <stdlib.h>
#include <stdio.h>

INT fois_deux(INT a) {
    return a*2;
}

INT divise_deux(INT a) {
    return a/2;
}

VOID apply(INT FUNC(f) (INT), INT tab[], INT tabLength) {
    INT i=0;
    while (i<tabLength) {
        tab[i] = f(tab[i]);
        i++;
    }
}

VOID afficher_tab(INT tab[], INT tabLength){
    INT i=0;
    while (i<tabLength)
    {
        printf("voici l'element %d du tableau : %d\n", i, tab[i]);
        i++;
    }   
}

VOID afficher_tabFloat(FLOAT tab[], INT tabLength){
    INT i=0;
    while (i<tabLength)
    {
        printf("voici l'element %d du tableau : %f\n", i, tab[i]);
        i++;
    }   
}

BOOLEAN test_pair(INT a){
    return !(a%2);
}

BOOLEAN test_impair(INT a){
    return a%2;
}

BOOLEAN test_negatif(INT a){
    return a<0;
}

BOOLEAN test_premier(INT a) {
    for (INT i = a - 1; i > 1; i--) {
        if (i == 1) {
            return FALSE;
        } else if (a % i == 0) {
            return FALSE;
        }
    }
    return TRUE;
}

VOID supprime_tab(INT tab[], UINT size, BOOLEAN FUNC(f) (INT), INT** resultTab, UINT* resultSize){

    UINT deleteCount = 0;
    for (UINT i = 0; i < size; i++) {
        if (f(tab[i])) {
            deleteCount++;
        }
        UINT sizeTab = size - deleteCount;
        INT* resultVal = (INT*)malloc(sizeof(INT) * (sizeTab));

        for (UINT i = 0, j = 0; i < size; i++) {
            if (!f(tab[i])) {
                resultVal[j] = tab[i];
                j++;
            }
        }

        *resultSize = sizeTab;
        *resultTab = resultVal;
    }
}

VOID tri_int(INT tab[], INT tabLength){
    INT c = 0;
    for(INT j=1;j<=tabLength;j++)
        for(INT i=0;i<tabLength-1;i++)
            if ( tab[i] > tab[i+1] ) {
                    c = tab[i];
                    tab[i] = tab[i+1];
                    tab[i+1] = c;
            }
}

VOID tri_float(FLOAT tab[], INT tabLength){
    FLOAT c = 0;
    for(INT j=1;j<=tabLength;j++)
        for(INT i=0;i<tabLength-1;i++)
            if ( tab[i] > tab[i+1] ) {
                    c = tab[i];
                    tab[i] = tab[i+1];
                    tab[i+1] = c;
            }
}